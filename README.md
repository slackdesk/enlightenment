# Enlightenment-e24

Enlightenment e24 desktop environment build scripts for Slackware Linux.


### What's here

The core of Enlightenment includes efl and enlightenment. On top of that are some added applications from the Enlightenment team, and their dependencies.

Core:  
efl - Enlightenment foundation libraries  
enlightenment - Window manager    

Apps:  
econnman - GUI frontend for connman (see KNOWN_ISSUES)  
ephoto - Image viewer  
epour - Torrent client  
evisum - Process and system monitor  
extra - Theme manager  
rage - Video player  
terminology - Terminal emulator    

Development:  
edi - Enlightenment IDE developer  
eflete - EFL Edje Theme Editor  
enventor - EDC file editor  


### Recommendations

Note from the Enlightenment team: It is not recommended to upgrade packages unless you uninstall and rebuild ALL packages to make sure that everything is linked correctly.  

Install these packages on a Slackware64 installation that has no other DE/WM's, or at least only install one DE/WM like XFCE, build and install Enlightenment, select Enlightenment as your DE with xwmconfig and then use slackpkg to remove XFCE. This will give you a more "clean" experience.


# 

Slackware® is a registered trademark of [Patrick Volkerding](http://www.slackware.com/)  
Linux® is a registered trademark of [Linus Torvalds](http://www.linuxmark.org/)
